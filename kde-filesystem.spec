%define _kde4_prefix %_prefix
%define _kde4_sysconfdir %_sysconfdir
%define _kde4_libdir %_libdir
%define _kde4_libexecdir %_libexecdir/kde4
%define _kde4_datadir %_datadir
%define _kde4_sharedir %_datadir
%define _kde4_iconsdir %_kde4_sharedir/icons
%define _kde4_configdir %_kde4_sharedir/config
%define _kde4_appsdir %_kde4_sharedir/kde4/apps
%define _kde4_docdir %_kde4_prefix/share/doc
%define _kde4_bindir %_kde4_prefix/bin
%define _kde4_sbindir %_kde4_prefix/sbin
%define _kde4_includedir %_kde4_prefix/include/kde4
%define _kde4_buildtype release
%define _kde4_macros_api 2

%define rpm_macros_dir %{_rpmconfigdir}/macros.d

Summary: KDE filesystem layout
Name: kde-filesystem
Version: 5
Release: 1

License: Public Domain

# noarch->arch transition
Obsoletes: kde-filesystem < 4-36

# teamnames (locales) borrowed from kde-i18n packaging
Source1: teamnames

Source2: macros.kde4
# increment whenever dirs change in an incompatible way
# kde4 apps built using macros.kde4 should

Source3: applnk-hidden-directory

BuildRequires: gawk

Requires:  filesystem
Requires:  rpm


%description
This package provides some directories that are required/used by KDE.

%package -n kde4-filesystem
Summary: Filesystem and RPM macros for KDE 4
License: Public Domain
Provides: kde4-macros(api) = %{_kde4_macros_api}
Requires: %{name} = %{version}-%{release}

%description -n kde4-filesystem
This package provides some directories that are required/used by KDE 4 applications.


%prep

%build


%install
## KDE4
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/rpm \
         $RPM_BUILD_ROOT%{_kde4_sysconfdir}/kde/{env,shutdown,kdm} \
         $RPM_BUILD_ROOT%{_kde4_includedir} \
         $RPM_BUILD_ROOT%{_kde4_libexecdir} \
         $RPM_BUILD_ROOT%{_kde4_appsdir}/color-schemes \
         $RPM_BUILD_ROOT%{_kde4_appsdir}/solid/actions \
         $RPM_BUILD_ROOT%{_kde4_datadir}/applications/kde4 \
         $RPM_BUILD_ROOT%{_kde4_datadir}/{autostart,wallpapers} \
         $RPM_BUILD_ROOT%{_kde4_configdir} \
         $RPM_BUILD_ROOT%{_kde4_sharedir}/config.kcfg \
         $RPM_BUILD_ROOT%{_kde4_sharedir}/emoticons \
         $RPM_BUILD_ROOT%{_kde4_sharedir}/kde4/services/ServiceMenus \
         $RPM_BUILD_ROOT%{_kde4_sharedir}/kde4/servicetypes \
         $RPM_BUILD_ROOT%{_kde4_sharedir}/templates/.source \
         $RPM_BUILD_ROOT%{_kde4_datadir}/icons/locolor/{16x16,22x22,32x32,48x48}/{actions,apps,mimetypes} \
         $RPM_BUILD_ROOT%{_kde4_docdir}/HTML/en/common
# do qt4 too?
# mkdir -p $RPM_BUILD_ROOT%{_prefix}/{lib,%{_lib}}/qt4/plugins
mkdir -p $RPM_BUILD_ROOT%{_kde4_prefix}/{lib,%{_lib}}/kde4/plugins/{gui_platform,styles}

for locale in $(grep '=' %{SOURCE1} | awk -F= '{print $1}') ; do
  mkdir -p $RPM_BUILD_ROOT%{_kde4_docdir}/HTML/${locale}/common
  echo "%lang($locale) %{_kde4_docdir}/HTML/$locale/" >> %{name}.list
done

# rpm macros
mkdir -p $RPM_BUILD_ROOT%{rpm_macros_dir}
cat >$RPM_BUILD_ROOT%{rpm_macros_dir}/macros.kde4<<EOF
%%_kde4_prefix %%_prefix
%%_kde4_sysconfdir %%_sysconfdir
%%_kde4_libdir %%_libdir
%%_kde4_libexecdir %%_libexecdir/kde4
%%_kde4_datadir %%_datadir
%%_kde4_sharedir %%_datadir
%%_kde4_iconsdir %%_kde4_sharedir/icons
%%_kde4_configdir %%_kde4_sharedir/config
%%_kde4_appsdir %%_kde4_sharedir/kde4/apps
%%_kde4_docdir %_kde4_prefix/share/doc
%%_kde4_bindir %%_kde4_prefix/bin
%%_kde4_sbindir %%_kde4_prefix/sbin
%%_kde4_includedir %%_kde4_prefix/include/kde4
%%_kde4_buildtype %_kde4_buildtype
%%_kde4_macros_api %_kde4_macros_api
EOF
cat %{SOURCE2} >> $RPM_BUILD_ROOT%{rpm_macros_dir}/macros.kde4

## Plasma5, forward compatibility
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/xdg/plasma-workspace/{env,shutdown}
mkdir -p $RPM_BUILD_ROOT%{_prefix}/{lib,%{_lib}}/kconf_update_bin
mkdir -p $RPM_BUILD_ROOT%{_datadir}/kconf_update
mkdir -p $RPM_BUILD_ROOT%{_datadir}/knsrcfiles
mkdir -p $RPM_BUILD_ROOT%{_datadir}/kpackage/{genericqml,kcms}
mkdir -p $RPM_BUILD_ROOT%{_datadir}/solid/{actions,devices}


%files -f %{name}.list
%{_sysconfdir}/xdg/plasma-workspace/
%{_prefix}/lib/kconf_update_bin/
%{_prefix}/%{_lib}/kconf_update_bin/
%dir %{_docdir}/HTML/
%lang(en) %{_docdir}/HTML/en/
%{_datadir}/config.kcfg/
%{_datadir}/emoticons/
%{_datadir}/icons/locolor/
%{_datadir}/kconf_update/
%{_datadir}/knsrcfiles/
%{_datadir}/kpackage/
%{_datadir}/solid/
%{_datadir}/templates/
%{_datadir}/wallpapers/

%files -n kde4-filesystem
%{rpm_macros_dir}/macros.kde4
%{_kde4_sysconfdir}/kde/
%{_kde4_libexecdir}/
%{_kde4_includedir}/
%{_kde4_appsdir}/
%{_kde4_configdir}/
%{_kde4_sharedir}/kde4/
%{_kde4_datadir}/applications/kde4/
%{_kde4_datadir}/autostart/
%{_kde4_prefix}/lib/kde4/
%{_kde4_prefix}/%{_lib}/kde4/

%changelog
* Tue Jan 02 2024 Paul Thomas <paulthomas100199@gmail.com> - 5-1
- update to version 5-1

* Sun Jan 19 2020 zhouyihang <zhouyihang1@huawei.com> - 4-62
- Remove kde3

* Thu Dec 12 2019 openEuler BuildTeam<buildteam@openeuler.org> - 4-61
- Package Init
